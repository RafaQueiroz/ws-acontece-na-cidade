package ifrs.progweb.teste;
import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Servidor {
public static String BASE_URL = "http://192.168.1.8:8004";
	
	public static HttpServer inicializaServidor(){
		URI uri = URI.create(BASE_URL);
		ResourceConfig config = new ResourceConfig().packages("ifrs.progweb.resources");	
		
		return GrizzlyHttpServerFactory.createHttpServer(uri, config);
	}
	
	public static void main(String[] args){
		
		HttpServer server = inicializaServidor();
		
		System.out.println("Servidor Rodando");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		server.stop();
	}
}

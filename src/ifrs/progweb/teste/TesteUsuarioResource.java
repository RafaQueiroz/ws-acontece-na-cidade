package ifrs.progweb.teste;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import ifrs.progweb.modelo.Usuario;


public class TesteUsuarioResource {

	private HttpServer server;
	private Client client;
	private WebTarget target;
	
	
	@Before
	public void startServer(){
		
		server = Servidor.inicializaServidor();
		ClientConfig config = new ClientConfig();
		this.client = ClientBuilder.newClient(config);
		this.target = client.target(Servidor.BASE_URL);
	}
	
	@Test
	public void testaBuscaUsuarioPorId(){
		
		String usuarioJSON = this.target.path("/usuarios/1").request().get(String.class);
		Usuario usuario = new Gson().fromJson(usuarioJSON, Usuario.class);
		
		Assert.assertEquals(usuario.getNome(), "Rafael Queiroz");
	}
		
	
	@Test
	public void testaInsercaoUsuario(){
		
		Usuario novoUsuario = new Usuario();
		novoUsuario.setNome("Teste");
		novoUsuario.setEmail("teste@teste.com");
		novoUsuario.setSenha("teste123");
		
		String novoUsuarioJSON = new Gson().toJson(novoUsuario);

		//Verificando se o usuário foi enviado
		Entity<String> entity  = Entity.entity(novoUsuarioJSON, MediaType.APPLICATION_JSON);
		Response response = target.path("/usuarios").request().post(entity);
		Assert.assertEquals(201, response.getStatus());
		
		//Verificando se o usuário foi inserido
		String usuarioLoaction = response.getHeaderString("Location");
		String usuarioJSON = this.client.target(usuarioLoaction).request().get(String.class);
		
		Usuario usuario = new Gson().fromJson(usuarioJSON, Usuario.class);
		Assert.assertEquals(usuario.getNome(), "Teste");
	}
	
	@Test
	public void testaAtualizacaoUsuario(){
		
		String usuarioJSON = this.target.path("/usuarios/2").request().get(String.class);
		Usuario usuarioAtualizado = new Gson().fromJson(usuarioJSON, Usuario.class);
		
		usuarioAtualizado.setNome("atualizado");

		String usuarioAtualizadoJSON = new Gson().toJson(usuarioAtualizado);
		//verifica se alteração foi feita
		Entity<String> entity = Entity.entity(usuarioAtualizadoJSON, MediaType.APPLICATION_JSON);
		Response response = this.target.path("/usuarios/1").request().put(entity);
		Assert.assertEquals(200, response.getStatus());
		
		//verifica se atualização foi persistida
		usuarioJSON = this.target.path("/usuarios/1").request().get(String.class);
		Usuario usuario = new Gson().fromJson(usuarioJSON, Usuario.class);
		Assert.assertEquals(usuario.getNome(), "atualizado");
		
		
	}
	
	@Test
	public void testaRemocaoUsuario(){
		Response response = this.target.path("/usuarios/5").request().delete();
		Assert.assertEquals(200, response.getStatus());
	}
	
	@After
    public void stopServer(){
            this.server.stop();
    }

}

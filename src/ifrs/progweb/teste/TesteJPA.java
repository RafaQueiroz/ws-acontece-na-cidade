package ifrs.progweb.teste;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ifrs.progweb.dao.EventoDao;
import ifrs.progweb.dao.UsuarioDao;
import ifrs.progweb.modelo.Evento;
import ifrs.progweb.modelo.Usuario;
import ifrs.progweb.util.HibernateUtil;

public class TesteJPA {
	public static void main(String args[]){
		
		EntityManager manager = HibernateUtil.getManager();
		UsuarioDao usuarioDao = new UsuarioDao();
		EventoDao eventoDao = new EventoDao();
		
		Usuario proprietario = new Usuario();
		proprietario.setNome("Rafael Queiroz");
		proprietario.setEmail("rafael@mail.com");
		proprietario.setSenha("qwe123");
				
		List<Usuario> participantes = new ArrayList<>();
		Calendar dataInicio = Calendar.getInstance();
		Calendar dataFim = Calendar.getInstance();
		
		Usuario participante1 = new Usuario();
		participante1.setNome("Jorge");
		participante1.setEmail("jorge@mail.com");
		participante1.setSenha("jorge123");
		participantes.add(participante1);
		
		Usuario participante2 = new Usuario();
		participante2.setNome("Marcio");
		participante2.setEmail("marcio@mail.com");
		participante2.setSenha("marcio123");
		participantes.add(participante2);
		
		usuarioDao.salvar(proprietario);
		usuarioDao.salvar(participante1);
		usuarioDao.salvar(participante2);
		
		Evento evento = new Evento();
		evento.setNome("Evento 1");
		evento.setDescricao("O primeiro Evento");
		evento.setProprietario(proprietario);
		evento.setParticipantes(participantes);
		evento.setDataInicio(dataInicio);
		evento.setDataInicio(dataInicio);
		evento.setDataFim(dataFim);
		
		eventoDao.salvar(evento);
		manager.close();
	}
}

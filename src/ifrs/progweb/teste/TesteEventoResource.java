package ifrs.progweb.teste;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ifrs.progweb.modelo.Evento;
import ifrs.progweb.modelo.Usuario;

public class TesteEventoResource {

	private HttpServer server;
	private Client client;
	private WebTarget target;
	
	
	@Before
	public void startServer(){
		
		server = Servidor.inicializaServidor();
		ClientConfig config = new ClientConfig();
		this.client = ClientBuilder.newClient(config);
		this.target = client.target(Servidor.BASE_URL);
	}
	
	@Test
	public void testaInsercaoEvento(){
	
		
		Evento novoEvento = new Evento();
		novoEvento.setNome("Liquida Casas Bahia");
		novoEvento.setDescricao("O pior liquida do Brasil");
		novoEvento.setDataInicio(Calendar.getInstance());
		novoEvento.setDataFim(Calendar.getInstance());
		
		List<Usuario> particiantes = new ArrayList<>();
		
		novoEvento.setParticipantes(particiantes);
		
		String novoEventoJSON = new Gson().toJson(novoEvento); 
		
		
		Entity<String> entity= Entity.entity(novoEventoJSON, MediaType.APPLICATION_JSON);
		Response response = this.target.path("/eventos").request()
				.header("Authorization", "Basic jorge@mail.com:jorge123").post(entity);
		Assert.assertEquals(201, response.getStatus());

		
		String eventoLocation = response.getHeaderString("Location");
		System.out.println("Location: "+eventoLocation);
		
		String eventoJSON = this.client.target(eventoLocation).request()
				.header("Authorization", "Basic jorge@mail.com:jorge123").get(String.class);
		
		Evento evento = new Gson().fromJson(eventoJSON, Evento.class);
		Assert.assertEquals(evento.getNome(), novoEvento.getNome());
		
	}
	
	@Test
	public void testaBuscarTodosEventos(){
		String eventoListJSON = this.target.path("/eventos").request()
				.header("Authorization", "Basic jorge@mail.com:jorge123").get(String.class);
		List<Evento> eventoList = new Gson().fromJson(eventoListJSON, new TypeToken<List<Evento>>(){}.getType());
		
		
		Assert.assertTrue(eventoListJSON.length() > 0);
	}
	
	
	@Test
	public void testaParticiparEvento(){
		
		Entity<String> entity= Entity.entity("", MediaType.APPLICATION_JSON);
		Response response = this.target.path("/eventos/9/participar").request()
				.header("Authorization", "Basic jorge@mail.com:jorge123").post(entity);
		Assert.assertEquals(200, response.getStatus());
		
	}
	
	@After
    public void stopServer(){
            this.server.stop();
    }
}

package ifrs.progweb.resources;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.List;
import ifrs.progweb.dao.EventoDao;
import ifrs.progweb.dao.UsuarioDao;
import ifrs.progweb.modelo.Evento;
import ifrs.progweb.modelo.Usuario;

@Path("eventos")
public class EventoResource {
	
	private EventoDao eventoDao;
	
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscar(@PathParam("id") Long id){
	
		if(id == null)
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("O id deve ser informado")
					.build();
		
		Evento evento = new EventoDao().encontrar(id);
		
		if(evento == null)
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("Evento não encontrado")
					.build();
			
			
		return Response.status(200)
				.entity(evento.toJson())
				.build();
		
	}
	
	// Adicionar Evento
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response adicionar(String eventoJSON, 
			@Context ContainerRequestContext request){
		
		try{
			Evento novoEvento = new Gson().fromJson(eventoJSON, Evento.class);
			
//			Usuario proprietario = (Usuario) request.getProperty("usuario");
			Usuario proprietario = new UsuarioDao()
					.getUsuarioPorEmail(novoEvento.getProprietario().getEmail());
			
			novoEvento.setProprietario(proprietario);

			new EventoDao().salvar(novoEvento);
			URI uri = URI.create("/eventos/"+novoEvento.getId());
			
			return Response.created(uri).build();
		
		} catch(JsonSyntaxException e){
			e.printStackTrace();
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("O Json não possui a formatação certa")
					.build();
		} catch (HibernateException e) {
			e.printStackTrace();
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Erro ao inserir no banco")
					.build();
		}
	}
	
	// Atualizar Evento
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editar(String eventoJSON, @PathParam("id") long id){
		
		eventoDao = new EventoDao();
		
		try{
			Evento eventoAtualizado = new Gson().fromJson(eventoJSON, Evento.class);
			Evento evento = eventoDao.encontrar(id);
			
			if(eventoAtualizado.getNome() != null)
				evento.setNome(eventoAtualizado.getNome());
			
			if(eventoAtualizado.getDescricao() != null)
				evento.setDescricao(eventoAtualizado.getDescricao());
			
			if(eventoAtualizado.getDataInicio() != null)
				evento.setDataFim(eventoAtualizado.getDataInicio());
			
			if(eventoAtualizado.getDataFim() != null)
				evento.setDataFim(eventoAtualizado.getDataFim());
			
			eventoDao.atualizar(evento);
			
			return Response.ok("Evento editado com sucesso").build();
		} catch(JsonSyntaxException e){
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("O Json não possui a formatação certa")
					.build();
		} catch (HibernateException e) {
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Erro ao inserir no banco")
					.build();
		}
		
		
	}
	
	// Remover Evento
	@DELETE
	@Path("/{id}")
	public Response remove(@PathParam("{id}") long id){
		
		try{
			new EventoDao().remover(id);
			return Response.ok("Evento Removido com sucesso").build();
		} catch (Exception e) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("Erro ao remover evento")
					.build();
		}
	}
	
	
	// Listar Eventos
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarTodos(){
		List<Evento> eventos = new EventoDao().getList();
		
		String eventosJSON = new Gson().toJson(eventos);
		
		return Response
				.status(200)
				.entity(eventosJSON)
				.build();
	}
	
	// Listar Participante 
	
	@GET
	@Path("/{id}/participantes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarParticipantes(@PathParam("id") Long id){
		
		if(id == null)
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("O id deve ser informado")
					.build();
		
		Evento evento = new EventoDao().encontrar(id);
		
		if(evento == null)
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("Nenhum evento foi encontrado com este id")
					.build();
		
		String participantesJSON = new Gson().toJson(evento.getParticipantes());
		
		return Response.status(Response.Status.OK)
				.entity(participantesJSON)
				.build();
	}
	
	// participar
	@POST
	@Path("/{id}/participar")
	public Response participar(@PathParam("id") Long id,
			@Context ContainerRequestContext request){
		
		try{
			Usuario usuario = (Usuario) request.getProperty("usuario");
			
			if(id == null)
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("O id deve ser informado")
						.build();
			
			Evento evento = new EventoDao().encontrar(id);
			
			if(evento == null)
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("Nenhum evento foi encontrado com este id")
						.build();
			
			List<Usuario> participantes = evento.getParticipantes();
			participantes.add(usuario);
			evento.setParticipantes(participantes);
			
			new EventoDao().atualizar(evento);
			
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.build();
		}
		
	}
	
	
	//deixar evento
	@POST
	@Path("/{id}/sair")
	public Response sair(@PathParam("id") Long id, 
			@Context ContainerRequestContext request){
		 
		Usuario participante = (Usuario) request.getProperty("usuario");
		
		if(id == null)
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("O id deve ser informado")
					.build();
		
		Evento evento = new EventoDao().encontrar(id);
		
		if(evento == null)
			return Response.status(Response.Status.BAD_REQUEST)
					.entity("Nenhum evento foi encontrado com este id")
					.build();
		
		evento.getParticipantes().remove(participante);
		
		new EventoDao().atualizar(evento);
		
		return Response.ok().build();
	}

}

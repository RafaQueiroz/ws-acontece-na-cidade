package ifrs.progweb.resources;


import java.net.URI;
import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import ifrs.progweb.dao.UsuarioDao;
import ifrs.progweb.erros.AppException;
import ifrs.progweb.modelo.Usuario;

@Path("/usuarios")
public class UsuarioResource {
	
	private static final Integer TOKEN_SIZE = 10;
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscaPorToken(@PathParam("id") Long id) throws AppException{
		

		if(id == null)
			throw new AppException(401, "O id precisa ser informado ");
		
		
		Usuario usuario = new UsuarioDao().encontrar(id);
			
		if(usuario == null)
			throw new AppException(401, "Nenhum usuário foi encontrado com esse id");
		
		return Response.status(200)
				.entity(usuario.toJson())
				.build();
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response loggar(String usuarioJSON){
		
		System.out.println("Começou Loggin...");
		
		if(usuarioJSON == null)
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("Credenciais não informadas")
					.build();
		
		System.out.println("Dados: "+usuarioJSON);
		
		try{
			Usuario usuario = new Gson().fromJson(usuarioJSON, Usuario.class);
			System.out.println("Converteu usuario");
			
			if(usuario.getEmail() == null || usuario.getEmail().isEmpty()||
					usuario.getSenha() == null || usuario.getSenha().isEmpty())
				return Response
						.status(Response.Status.BAD_REQUEST)
						.entity("Credenciais não informadas")
						.build();
			
			System.out.println("Email e senha informado");
			
			System.out.println("Buscando Usuario pelo email");
			Usuario usuarioRegistrado = new UsuarioDao()
					.getUsuarioPorEmail(usuario.getEmail());
			
			if(usuarioRegistrado == null)
				return Response
						.status(Response.Status.BAD_REQUEST)
						.entity("E-mail ou senha incorreto")
						.build();
			System.out.println("Encontrou. Validando senha ...");
			System.out.println(usuario.getSenha() + " - " + usuarioRegistrado.getSenha());
			if(!usuarioRegistrado.getSenha().equals(usuario.getSenha()))
				return Response
						.status(Response.Status.BAD_REQUEST)
						.entity("E-mail ou senha incorreto")
						.build();
			
			System.out.println("ok!");
			
			return Response.ok(usuarioRegistrado.toJson()).build();
		} catch (JsonSyntaxException e) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("O JSON não possui a estrutura certa")
					.build();
		} catch(Exception e){
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Erro ao processar informação - "+e.getMessage())
					.build();
		}
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response adicionar(String usuarioJson) throws AppException{
		
		Usuario novoUsuario = new Gson().fromJson(usuarioJson, Usuario.class);
		
		if(novoUsuario == null)
			throw new AppException(401, "Credenciais não informadas");
		
		if(novoUsuario.getEmail() == null || novoUsuario.getSenha() == null 
				|| novoUsuario.getNome() == null)
			throw new AppException(401, "Campos obrigatórios não foram informados");
		
		
		new UsuarioDao().salvar(novoUsuario);
		
		URI novoUsuarioLocation = URI.create("/usuarios/"+novoUsuario.getId());
		return Response.created(novoUsuarioLocation)
				.build();
	}
	
	private String genAccessToken(){
		char[] chars = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		
		Random random = new Random();
		StringBuilder token = new StringBuilder();
		
		for(int i =0; i <TOKEN_SIZE; i++){
			char c = chars[random.nextInt(chars.length)];
			token.append(c);
		}
		
		return token.toString();
	}
	
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response atualiza(String usuarioAtualizadoJSON,
			@PathParam("id") Long id) throws AppException{
	
		if(id == null)
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("O id do usuário deve ser informado")
					.build();
		
		
		try{
			Usuario usuarioAtualizado = new Gson().fromJson(usuarioAtualizadoJSON, Usuario.class);
			
			Usuario usuario = new UsuarioDao().encontrar(id);
			
			if(usuario == null)
				return Response
						.status(Response.Status.BAD_REQUEST)
						.entity("usuario não encontrado")
						.build();
			
			if(usuarioAtualizado.getEmail() != null)
				usuario.setEmail(usuarioAtualizado.getEmail());
			
			if(usuarioAtualizado.getSenha() != null)
				usuario.setSenha(usuarioAtualizado.getSenha());
			
			if(usuarioAtualizado.getNome() != null)
				usuario.setNome(usuarioAtualizado.getNome());
				
			new UsuarioDao().atualizar(usuario);
			
			return Response.ok("Usuario atualizado com sucesso").build();
		
		} catch (JsonSyntaxException e) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("O json não possui o formato correto")
					.build();
		} catch (HibernateException e) {
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(e.getMessage())
					.build();
		}
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") Long id) throws AppException{
		UsuarioDao usuarioDao = new UsuarioDao();
		
		if(id == null)
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity("O id do usuário deve ser informado")
					.build();
				
		usuarioDao.remover(id);
		return Response.ok().build();
	}
	
}

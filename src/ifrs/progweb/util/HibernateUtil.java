package ifrs.progweb.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {
	
	public static EntityManager getManager(){
		return Persistence.createEntityManagerFactory("ifrs.progweb.aconteceNaCidade.mysql")
				.createEntityManager();
	}
}

package ifrs.progweb.dao;

import ifrs.progweb.modelo.Evento;

public class EventoDao extends GenericDao<Evento, Long>{
	public EventoDao(){
		super(Evento.class);
	}
	
}

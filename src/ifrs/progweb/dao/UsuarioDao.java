package ifrs.progweb.dao;

import javax.persistence.TypedQuery;

import ifrs.progweb.modelo.Usuario;

public class UsuarioDao extends GenericDao<Usuario, Long>{
	public UsuarioDao(){
		super(Usuario.class);
	}

	public Usuario getUsuarioPorEmail(String email) {
		 TypedQuery<Usuario> query = entityManager
				 .createNamedQuery("userByEmail", Usuario.class);
		 
		 query.setParameter("email", email);
		 
		 Usuario usuario = (Usuario) query.getSingleResult(); 
		 
		 
		 
		 return usuario;
	}

}

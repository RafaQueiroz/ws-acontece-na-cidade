package ifrs.progweb.erros;

public class AppException extends Exception{

	private static final long serialVersionUID = -8999932578270387947L;
	
	/*
	 * HTTP response status*/
	Integer status;
	
	public AppException(int status, String mensagem){
		super(mensagem);
		
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}

package ifrs.progweb.erros;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class AppExceptionMapper implements ExceptionMapper<AppException>{

	@Override
	public Response toResponse(AppException e) {
		// TODO Auto-generated method stub
		return Response.status(e.getStatus())
				.entity(e.getMessage())
				.type(MediaType.APPLICATION_JSON)
				.build();
	}

}
